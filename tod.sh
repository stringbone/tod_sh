#!/bin/bash

# Bring in all constants and helper functions
. "$(dirname "$0")/tod.h.sh"

init

if [[ "$1" =~ ^[0-9]+$ ]]; then
    task_number="$1"
    number_of_tasks=$(wc -l < "$TOD_FILE")
    if [[ "$task_number" -gt "$number_of_tasks" ]]; then
        echo "There is no task ${task_number}."
        exit 1
    fi
    do_pomodoro "$1"
else
    command="$1"
    shift # remove action from args

    case $command in
    a)  add_tasks "$@" ;;
    aa) add_and_do "$1" ;;
    b)  take_break ;;
    c)
        # If no tasks given to mark complete, equivalent to `lc`
        if [[ -z $1 ]]; then
            list_tasks "completed"
        else
            mark_complete "$@"
        fi
        ;;
    e)  $EDITOR "$TOD_FILE" ;;
    h)  $EDITOR "$(dirname "$0")/README.md" ;;
    k)  kill_existing_timers ;;
    lc) list_tasks "completed" ;;
    ls) list_tasks "all" ;;
    p)  list_tasks "project" "$1" ;;
    t)
        if [[ -z $1 ]]; then
            time_left
        else
            touch_task "$@"
            list_tasks
        fi
        ;;
    '') list_tasks ;;
    *)  echo "?" && exit 1 ;;
    esac
fi

